FROM docker:20-dind as upstream
FROM scratch
COPY --from=upstream / /
VOLUME /var/lib/docker
# do not expose 2375
EXPOSE 2376
ENTRYPOINT ["dockerd-entrypoint.sh"]
CMD []
