
# DEPRECATED

This solution is no longer necessary.

# TLS-only docker-in-docker

A clone of `docker:20-dind`, but with port 2375 not exposed.

This is intended for use in Gitlab CI to work around the health check problem. Image is rebuilt from
`docker:20-dind` weekly to ensure it stays up to date.

See https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29130 for rationale

To use, change this:

```yml
services:
  - docker:20-dind
```

to this:

```yml
services:
  - name: francisferrell/docker:20-dind
    alias: docker
```

# Links

- Source: https://gitlab.com/francisferrell/docker-in-docker
- Docker Hub: https://hub.docker.com/r/francisferrell/docker

